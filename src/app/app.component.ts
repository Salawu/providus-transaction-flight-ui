import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import * as EventBus from 'vertx3-eventbus-client';

import { HomePage } from '../pages/home/home';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = HomePage;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
      
      let eb = new EventBus("http://connect.isec.ng/connect");
     eb.onopen = function(){
console.log("Eventbbus connected");

     
      eb.registerHandler("out.log",null,function(err,msg){
        console.log(msg);
      });

       eb.registerHandler("monitoring.localbank.log",null,function(err,msg){
        console.log("monitoring.localbank.log : "+msg);
      });

       eb.registerHandler("clients.log",null,function(err,msg){
        console.log("clients.log : "+JSON.parse(msg.body)['mobile-udk']);
      });

       eb.registerHandler("monitoring.cloud.log",null,function(err,msg){
        console.log("monitoring.cloud.log : "+ msg);
      });

       eb.registerHandler("monitoring.ussd.log",null,function(err,msg){
        console.log("monitoring.ussd.log : "+msg);
      });

       eb.registerHandler("monitoring.push.log",null,function(err,msg){
        console.log("monitoring.push.log : "+msg);
      });


     };
    });
    
  }
}

